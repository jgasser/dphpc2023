#ifndef GEMVER_MPI_OPENMP_H
#define GEMVER_MPI_OPENMP_H

// Function to perform the GEMVER operation
void gemver_mpi_3_new_openmp(int n, double *A_result, double *x_result, double *w_result);
#endif